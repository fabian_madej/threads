import java.io.IOException;
import java.nio.CharBuffer;

public class Counter implements Runnable {

    public void run() {
        System.out.println(Counter.class.getName());
        synchronized (this) {
            for (int i = 1; i <= 10; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(i);
            }
        }
    }
}
