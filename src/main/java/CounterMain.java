public class CounterMain {
    public static void main(String[] args) {

        Thread thread1 = new Thread(new Counter());
        Thread thread2 = new Thread(new Counter());
        Thread thread3 = new Thread(new Counter());

        thread1.start();
        thread2.start();

        thread3.start();
    }
}
